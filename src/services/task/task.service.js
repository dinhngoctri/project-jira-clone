import { https } from "../configURL/configURL";

export const taskService = {
  getTaskTypeGetAll: () => https.get("/api/TaskType/getAll"),
  getPriorityGetAll: () => https.get("/api/Priority/getAll?id=0"),
  getStatusGetAll: () => https.get("/api/Status/getAll"),
  getProjectGetTaskDetail: (taskId) =>
    https.get(`/api/Project/getTaskDetail?taskId=${taskId}`),
  getCommentGetAll: (taskId) => https.get(`/api/Comment/getAll?taskId=${taskId}`),

  postProjectCreateTask: (taskInfo) => https.post("/api/Project/createTask", taskInfo),
  postProjectUpdateTask: (taskInfo) => https.post("/api/Project/updateTask", taskInfo),
  postProjectAssignUserTask: (assignData) =>
    https.post("/api/Project/assignUserTask", assignData),
  postCommentInsertComment: (comment) =>
    https.post("/api/Comment/insertComment", comment),

  putCommentUpdateComment: (commentId, contentComment) =>
    https.put(
      `/api/Comment/updateComment?id=${commentId}&contentComment=${contentComment}`
    ),
  putProjectUpdateStatus: (taskInfo) => https.put("/api/Project/updateStatus", taskInfo),

  deleteProjectRemoveTask: (taskId) =>
    https.delete(`/api/Project/removeTask?taskId=${taskId}`),
  deleteCommentDeleteComment: (commentId) =>
    https.delete(`/api/Comment/deleteComment?idComment=${commentId}`),
};
