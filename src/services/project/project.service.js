import { https } from "../configURL/configURL";

export const projectService = {
  getProjectGetAllProject: () => https.get("/api/Project/getAllProject"),
  getProjectCategory: () => https.get("/api/ProjectCategory"),
  getProjectGetProjectDetail: (projectID) =>
    https.get(`/api/Project/getProjectDetail?id=${projectID}`),

  postProjectCreateProjectAuthorize: (projectInfo) =>
    https.post("/api/Project/createProjectAuthorize", projectInfo),
  postProjectAssignUserProject: (assignData) =>
    https.post("/api/Project/assignUserProject", assignData),
  postProjectRemoveUserFromProject: (removeData) =>
    https.post("/api/Project/removeUserFromProject", removeData),

  putProjectUpdateProject: (projectID, projectInfo) =>
    https.put(`/api/Project/updateProject?projectId=${projectID}`, projectInfo),

  deleteProjectDeleteProject: (projectID) =>
    https.delete(`/api/Project/deleteProject?projectId=${projectID}`),
};
