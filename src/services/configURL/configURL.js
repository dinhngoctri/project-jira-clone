import axios from "axios";
import { store } from "../..";
import { setLoading } from "../../redux/layoutSlice/layoutSlice";
import { userLocalStorage } from "../user/user.localStorage";

const callingWithoutLoadingScreen = [
  "/api/Comment/",

  "/api/ProjectCategory",
  "/api/TaskType/getAll",
  "/api/Priority/getAll?id=0",
  "/api/Status/getAll",

  "/api/Project/getTaskDetail?taskId=",
  "/api/Project/getProjectDetail?id=",
  "/api/Project/assignUserProject",
  "/api/Project/removeUserFromProject",
];

const tokenCybersoft =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzMSIsIkhldEhhblN0cmluZyI6IjIzLzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3NzExMDQwMDAwMCIsIm5iZiI6MTY0ODQwMDQwMCwiZXhwIjoxNjc3MjU4MDAwfQ.0byoDjBIIS6877xg7NwEnO16v5HOltI9AatD9OLB0Ys";

export const https = axios.create({
  baseURL: "https://jiranew.cybersoft.edu.vn",
  headers: {
    "Content-Type": "application/json",
    TokenCybersoft: tokenCybersoft,
  },
  transformRequest: [
    function (data, headers) {
      const user = userLocalStorage.userInfo.get();
      const getAccessToken = () => (user ? user.accessToken : "");

      headers["Authorization"] = `Bearer ${getAccessToken()}`;
      return JSON.stringify(data);
    },
  ],
});

https.interceptors.request.use(
  function (config) {
    const noNeedLoading = callingWithoutLoadingScreen.findIndex((url) =>
      config.url.includes(url)
    );

    noNeedLoading === -1 && store.dispatch(setLoading(true));
    return config;
  },
  function (error) {
    store.dispatch(setLoading(false));
    return Promise.reject(error);
  }
);

https.interceptors.response.use(
  function (response) {
    const noNeedLoading = callingWithoutLoadingScreen.findIndex((url) =>
      response.config.url.includes(url)
    );

    noNeedLoading === -1 && store.dispatch(setLoading(false));
    return response;
  },
  function (error) {
    const noNeedLoading = callingWithoutLoadingScreen.findIndex((url) =>
      error.config.url.includes(url)
    );

    noNeedLoading === -1 && store.dispatch(setLoading(false));
    return Promise.reject(error);
  }
);
