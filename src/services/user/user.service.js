import { https } from "../configURL/configURL";

export const userService = {
  postUserSignIn: (userInput) => https.post("/api/Users/signin", userInput),
  postUserSignUp: (userInput) => https.post("/api/Users/signup", userInput),

  putEditUser: (userInput) => https.put("/api/Users/editUser", userInput),

  getUsersGetUser: () => https.get("/api/Users/getUser"),

  deleteUserDeleteUser: (userId) => https.delete(`/api/Users/deleteUser?id=${userId}`),
};
