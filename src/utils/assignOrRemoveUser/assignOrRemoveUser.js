import { assignUserToProject } from "../projectAction/assignUserToProject";
import { removeUserFromProject } from "../projectAction/removeUserFromProject";

export const assignOrRemoveUser = (
  newMemberList = [],
  currentMemberList = [],
  projectId,
  type = "project",
  reloadData = () => {}
) => {
  const newListLength = newMemberList.length;
  const currentListLength = currentMemberList.length;

  let newMemberIndex = 0;
  let currentMemberIndex = 0;
  let apiResponeTimes = 0;
  let apiRequestTimes = 0;

  const afterAssignOrRemove = () => {
    apiResponeTimes++;
  };
  const assign = (data, lastAssign) => {
    if (type === "project") {
      lastAssign
        ? assignUserToProject(data, reloadData)
        : assignUserToProject(data, afterAssignOrRemove);
    }
  };
  const remove = (data, lastRemove) => {
    if (type === "project") {
      lastRemove
        ? removeUserFromProject(data, reloadData)
        : removeUserFromProject(data, afterAssignOrRemove);
    }
  };
  const determineWhenToReloadData = setInterval(() => {
    if (apiRequestTimes === apiResponeTimes) {
      reloadData();
      clearInterval(determineWhenToReloadData);
    }
  }, 50);

  const newListSorted = newMemberList.sort((a, b) => a - b);
  const currentListSorted = currentMemberList.sort((a, b) => a - b);

  const data = (memberId) => ({ projectId, userId: memberId });
  if (newListLength > 0 && currentListLength > 0) {
    // start loop
    for (let i = 0; i < 100; i++) {
      const newMemberId = newListSorted[newMemberIndex];
      const currentMemberId = currentListSorted[currentMemberIndex];

      if (newMemberIndex < newListLength && currentMemberIndex < currentListLength) {
        if (newMemberId < currentMemberId) {
          assign(data(newMemberId));
          apiRequestTimes++;
          newMemberIndex++;
        } else if (newMemberId > currentMemberId) {
          remove(data(currentMemberId));
          apiRequestTimes++;
          currentMemberIndex++;
        } else {
          newMemberIndex++;
          currentMemberIndex++;
        }
      } else {
        const restRemoved = currentListSorted.slice(currentMemberIndex);
        const restAssigned = newListSorted.slice(newMemberIndex);

        const removeLength = restRemoved.length;
        const assignLength = restAssigned.length;

        if (newMemberIndex >= newListLength) {
          restRemoved.forEach((memberId, index) => {
            index >= removeLength - 1
              ? remove(data(memberId), true)
              : remove(data(memberId));
          });
          return;
        }

        if (currentMemberIndex >= currentListLength) {
          restAssigned.forEach((memberId, index) => {
            index >= assignLength - 1
              ? assign(data(memberId), true)
              : assign(data(memberId));
          });
          return;
        } else {
          return;
        }
      }
    }
    // end loop
  } else {
    if (newListLength <= 0) {
      currentListSorted.forEach((memberId, index) => {
        index >= currentListSorted.length - 1
          ? remove(data(memberId), true)
          : remove(data(memberId));
      });
    } else {
      newListSorted.forEach((memberId, index) => {
        index >= newListSorted.length - 1
          ? assign(data(memberId), true)
          : assign(data(memberId));
      });
    }
  }
};
