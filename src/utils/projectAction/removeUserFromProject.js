import { projectService } from "../../services/project/project.service";

export const removeUserFromProject = (removeData, onSuccess = () => {}) => {
  projectService
    .postProjectRemoveUserFromProject(removeData)

    .then(() => {
      onSuccess();
    })

    .catch(() => {});
};
