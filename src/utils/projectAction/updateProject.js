import { projectService } from "../../services/project/project.service";
import { openNotification } from "../openNotification/openNotification";

export const updateProject = (projectID, projectInfo, onSuccess) => {
  projectService
    .putProjectUpdateProject(projectID, projectInfo)

    .then((success) => {
      onSuccess(success);

      openNotification({
        type: "success",
        message: "Update success!",
        description: "Saving your changes.",
      });
    })

    .catch((error) => {
      openNotification({
        type: "error",
        message: "Update fail!",
        description: error.response.data.content,
      });
    });
};
