import { projectService } from "../../services/project/project.service";
import { openNotification } from "../openNotification/openNotification";

export const deleteProject = (projectID, onSuccess) => {
  projectService
    .deleteProjectDeleteProject(projectID)

    .then(() => {
      openNotification({
        type: "success",
        message: "Delete success!",
        description: "Redirects you to homepage.",
      });
      onSuccess();
    })

    .catch((error) => {
      openNotification({
        type: "error",
        message: "Delete fail!",
        description: error.response.data.content,
      });
    });
};
