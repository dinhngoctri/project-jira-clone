export const filterMyProject = (allProject, userInfo) => {
  const { id } = userInfo;

  return (
    allProject &&
    allProject.filter((project) => {
      const { members, creator } = project;
      const isMember = members.findIndex((item) => item.userId === id);
      return isMember !== -1 || creator.id === id;
    })
  );
};
