import { projectService } from "../../services/project/project.service";
import { openNotification } from "../openNotification/openNotification";
import { getAllProject } from "./getAllProject";

export const createProject = (projectInfo, onSuccess) => {
  return (dispatch) => {
    projectService
      .postProjectCreateProjectAuthorize(projectInfo)

      .then((success) => {
        dispatch(getAllProject());
        onSuccess(success.data.content);

        openNotification({
          type: "success",
          message: "Create project success!",
          description: "Getting your project.",
        });
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Create project fail!",
          description: error.response.data.content,
        });
      });
  };
};
