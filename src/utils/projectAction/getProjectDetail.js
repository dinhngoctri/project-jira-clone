import { setProjectDetail } from "../../redux/projectSlice/projectSlice";
import { projectService } from "../../services/project/project.service";
import { openNotification } from "../openNotification/openNotification";

export const getProjectDetail = (projectID) => {
  return (dispatch) => {
    projectService
      .getProjectGetProjectDetail(projectID)

      .then((success) => {
        dispatch(setProjectDetail(success.data.content));
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Get project fail!",
          description: error.response.data.message,
        });
      });
  };
};
