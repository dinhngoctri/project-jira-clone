export const getTextFromHtmlString = (string) => {
  const span = document.createElement("span");
  span.innerHTML = string;
  return span.textContent || span.innerText;
};
