export const highlights = (string, highlightIndex) => {
  const { start, end } = highlightIndex;
  const highlightContent = string.slice(start, end + 1);

  const content = string.replace(
    highlightContent,
    `<span class='bg-yellow-300'>${highlightContent}</span>`
  );

  return <span dangerouslySetInnerHTML={{ __html: content }} />;
};
