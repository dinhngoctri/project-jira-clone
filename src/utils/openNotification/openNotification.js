import { notification } from "antd";

const defaultClass = "m-0 text-lg";

export const openNotification = ({ type, message, description }) => {
  const getType = () => {
    switch (type) {
      case "success":
        return { backgroundColor: "rgb(59 130 246)", color: "white" };

      case "error":
        return { backgroundColor: "rgb(185 28 28)", color: "white" };

      default:
        return { color: "rgb(59 130 246)" };
    }
  };

  notification.open({
    message: (
      <p
        className={
          type && type !== "default"
            ? defaultClass + " text-white"
            : defaultClass + " text-blue-500"
        }
      >
        {message}
      </p>
    ),

    description: <p className={defaultClass}>{description}</p>,
    style: getType(),
  });
};
