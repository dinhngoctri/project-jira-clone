import { useDispatch } from "react-redux";
import { showDrawer } from "../../redux/layoutSlice/layoutSlice";

export const useShowDrawer = () => {
  const dispatch = useDispatch();
  const handleShowDrawer = (drawerinfo) => {
    dispatch(showDrawer(drawerinfo));
  };

  return handleShowDrawer;
};
