export const translateEN = (string) => {
  switch (string.toLowerCase()) {
    case "dự án web":
      return "web project";

    case "dự án phần mềm":
      return "software project";

    case "dự án di động":
      return "mobile project";

    default:
      return string;
  }
};
