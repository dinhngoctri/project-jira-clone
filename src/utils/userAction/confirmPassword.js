import { userService } from "../../services/user/user.service";
import { openNotification } from "../openNotification/openNotification";

export const confirmPassword = (userInfo, onSuccess) => {
  userService
    .postUserSignIn(userInfo)

    .then(() => {
      onSuccess();
      openNotification({
        type: "success",
        message: "Confirm success!",
        description: "Now you can update info.",
      });
    })

    .catch((error) => {
      openNotification({
        type: "error",
        message: "Can't confirm your password!",
        description: error.response.data.message,
      });
    });
};
