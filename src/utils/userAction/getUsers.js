import { userService } from "../../services/user/user.service";
import { openNotification } from "../openNotification/openNotification";

export const getUsers = (onSuccess) => {
  userService
    .getUsersGetUser()

    .then((success) => {
      onSuccess(success.data.content);
    })

    .catch((error) => {
      openNotification({
        type: "error",
        message: "Get all user fail!",
        description: error.response.data.content,
      });
    });
};
