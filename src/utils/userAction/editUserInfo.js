import { userService } from "../../services/user/user.service";
import { openNotification } from "../openNotification/openNotification";

export const userEditInfo = (userInput, onSuccess) => {
  userService
    .putEditUser(userInput)

    .then(() => {
      onSuccess();
      openNotification({
        type: "success",
        message: "Update success!",
        description: "Getting new info.",
      });
    })

    .catch((error) => {
      openNotification({
        type: "error",
        message: "Update fail!",
        description: error.response.data.message,
      });
    });
};
