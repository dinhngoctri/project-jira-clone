import { userService } from "../../services/user/user.service";
import { openNotification } from "../openNotification/openNotification";

export const deleteUser = (userId, onSuccess) => {
  userService
    .deleteUserDeleteUser(userId)

    .then(() => {
      onSuccess();
      openNotification({
        type: "error",
        message: "Account deleted!",
        description: "Redirects you to login page.",
      });
    })

    .catch((error) => {
      openNotification({
        type: "error",
        message: "Delete fail!",
        description: error.response.data.message,
      });
    });
};
