import { setTimeTracking } from "../../redux/taskSlice/taskSlice";
import { taskService } from "../../services/task/task.service";
import { openNotification } from "../openNotification/openNotification";

export const getTaskDetail = (taskId, onSuccess = () => {}) => {
  return (dispatch) => {
    taskService
      .getProjectGetTaskDetail(taskId)

      .then((success) => {
        dispatch(setTimeTracking(success.data.content));
        onSuccess(success.data.content);
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Get task detail fail!",
          description: error.response.data.content,
        });
      });
  };
};
