import { closeDrawer } from "../../redux/layoutSlice/layoutSlice";
import { taskService } from "../../services/task/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getProjectDetail } from "../projectAction/getProjectDetail";

export const updateTask = (taskInfo) => {
  return (dispatch) => {
    taskService
      .postProjectUpdateTask(taskInfo)

      .then((success) => {
        const { projectId } = success.data.content;

        dispatch(getProjectDetail(projectId));
        dispatch(closeDrawer());
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Update task fail!",
          description: error.response.data.message,
        });
      });
  };
};
