import { setAllStatus } from "../../redux/taskSlice/taskSlice";
import { taskService } from "../../services/task/task.service";
import { openNotification } from "../openNotification/openNotification";

export const getAllStatus = () => {
  return (dispatch) => {
    taskService
      .getStatusGetAll()

      .then((success) => {
        dispatch(setAllStatus(success.data.content));
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Get all status fail!",
          description: error.response.data.message,
        });
      });
  };
};
