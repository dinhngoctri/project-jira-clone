import { taskService } from "../../services/task/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getAllComment } from "./getAllComment";

export const updateComment = (commentId, contentComment) => {
  return (dispatch) => {
    taskService
      .putCommentUpdateComment(commentId, contentComment)

      .then((success) => {
        dispatch(getAllComment(success.data.content.taskId));
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Edit comment fail!",
          description: error.response.data.message,
        });
      });
  };
};
