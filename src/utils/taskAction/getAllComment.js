import { setCommentList } from "../../redux/taskSlice/taskSlice";
import { taskService } from "../../services/task/task.service";
import { openNotification } from "../openNotification/openNotification";

export const getAllComment = (taskId) => {
  return (dispatch) => {
    taskService
      .getCommentGetAll(taskId)

      .then((success) => {
        dispatch(setCommentList(success.data.content));
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Get comments fail!",
          description: error.response.data.content,
        });
      });
  };
};
