import { setAllTaskType } from "../../redux/taskSlice/taskSlice";
import { taskService } from "../../services/task/task.service";
import { openNotification } from "../openNotification/openNotification";

export const getAllTaskType = () => {
  return (dispatch) => {
    taskService
      .getTaskTypeGetAll()

      .then((success) => {
        dispatch(setAllTaskType(success.data.content));
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Get all task type fail!",
          description: error.response.data.message,
        });
      });
  };
};
