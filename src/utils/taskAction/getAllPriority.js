import { setAllPriority } from "../../redux/taskSlice/taskSlice";
import { taskService } from "../../services/task/task.service";
import { openNotification } from "../openNotification/openNotification";

export const getAllPriority = () => {
  return (dispatch) => {
    taskService
      .getPriorityGetAll()

      .then((success) => {
        dispatch(setAllPriority(success.data.content));
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Get all priority fail!",
          description: error.response.data.message,
        });
      });
  };
};
