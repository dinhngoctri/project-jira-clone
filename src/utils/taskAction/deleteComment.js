import { taskService } from "../../services/task/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getAllComment } from "./getAllComment";

export const deleteComment = (commentId, taskId) => {
  return (dispatch) => {
    taskService
      .deleteCommentDeleteComment(commentId)

      .then(() => {
        dispatch(getAllComment(taskId));
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Delete comment fail!",
          description: error.response.data.content,
        });
      });
  };
};
