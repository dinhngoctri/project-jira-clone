import { taskService } from "../../services/task/task.service";
import { openNotification } from "../openNotification/openNotification";

export const deleteTask = (taskId, onSuccess) => {
  taskService
    .deleteProjectRemoveTask(taskId)

    .then(() => {
      onSuccess();
    })

    .catch((error) => {
      openNotification({
        type: "error",
        message: "Delete task fail!",
        description: error.response.data.content,
      });
    });
};
