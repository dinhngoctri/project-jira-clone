import { taskService } from "../../services/task/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getAllComment } from "./getAllComment";

export const insertComment = (comment) => {
  return (dispatch) => {
    taskService
      .postCommentInsertComment(comment)

      .then((success) => {
        dispatch(getAllComment(success.data.content.taskId));
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Comment fail!",
          description: error.response.data.message,
        });
      });
  };
};
