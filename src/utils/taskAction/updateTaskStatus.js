import { taskService } from "../../services/task/task.service";

export const updateTaskStatus = (taskInfo) => {
  taskService
    .putProjectUpdateStatus(taskInfo)

    .then(() => {})

    .catch(() => {});
};
