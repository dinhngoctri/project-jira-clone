import { taskService } from "../../services/task/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getProjectDetail } from "../projectAction/getProjectDetail";

export const createTask = (taskInfo, onSuccess) => {
  return (dispatch) => {
    taskService
      .postProjectCreateTask(taskInfo)

      .then((success) => {
        const { projectId } = success.data.content;
        dispatch(getProjectDetail(projectId));

        onSuccess();

        openNotification({
          type: "success",
          message: "Create task success!",
          description: "Getting your task.",
        });
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Create task fail!",
          description: error.response.data.content,
        });
      });
  };
};
