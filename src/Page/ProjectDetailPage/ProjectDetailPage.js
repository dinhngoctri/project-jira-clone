import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { FormOutlined } from "@ant-design/icons";
import { Tooltip } from "antd";
import TagProjectCategory from "../../Components/Utils/Tag/TagProjectCategory";
import { NavLink, useParams } from "react-router-dom";
import TagArrayMember from "../../Components/Utils/TagArray/TagArrayMember";
import { useDispatch } from "react-redux";
import { setBreadcrumbPath } from "../../redux/layoutSlice/layoutSlice";
import { myBreadcrumbPath } from "../../Components/Utils/MyBreadcrumb/myBreadcrumbPath";

export default function ProjectDetailPage() {
  const dispatch = useDispatch();
  const { projectID } = useParams();
  const { userInfo } = useSelector((state) => state.userSlice);
  const { name: userName } = userInfo;
  const { projectDetail } = useSelector((state) => state.projectSlice);
  const { projectName, projectCategory, description } = projectDetail;

  const { id, name } = projectCategory;
  const newProjectCategory = { id, projectCategoryName: name };

  useEffect(() => {
    dispatch(
      setBreadcrumbPath(
        myBreadcrumbPath.projectDetailPage(userName, projectID, projectName)
      )
    );
    return () => {
      dispatch(setBreadcrumbPath(null));
    };
  }, [dispatch, userName, projectID, projectName]);

  return (
    <>
      <div className="flex items-center mb-6">
        <p className="m-0 mr-3 text-3xl font-medium">{projectName}</p>
        <div className="flex items-center mt-1">
          <NavLink to={`/project-${projectID}/project-detail/edit-project`}>
            <Tooltip
              title="Edit project"
              className="cursor-pointer text-lg transition duration-300 hover:text-blue-500"
            >
              <FormOutlined />
            </Tooltip>
          </NavLink>
        </div>
      </div>
      <div className="w-fit mb-6">
        <TagProjectCategory category={newProjectCategory} />
      </div>

      <div className="w-3/4 flex flex-wrap mb-12" style={{ marginLeft: 2 }}>
        <TagArrayMember
          projectDetail={projectDetail}
          maxTag={50}
          marginLeft={-2}
          scale={1.25}
        />
      </div>

      <div className="w-full" dangerouslySetInnerHTML={{ __html: description }} />
    </>
  );
}
