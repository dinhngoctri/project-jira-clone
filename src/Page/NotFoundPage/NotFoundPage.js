import React from "react";
import Lottie from "lottie-react";
import notFoundPage from "../../asset/animation/not-found-page.json";
import TypewriterComponent from "typewriter-effect";
import { NavLink } from "react-router-dom";
import { Button } from "antd";

export default function NotFoundPage() {
  return (
    <>
      <div
        className="min-h-screen flex flex-col items-center"
        style={{ backgroundColor: "#f8f8f8" }}
      >
        <Lottie className="w-screen lg:w-2/3" animationData={notFoundPage} loop={true} />

        <div className="flex flex-col items-center py-10 px-10 lg:px-48 space-y-5">
          <TypewriterComponent
            onInit={(typewriter) => {
              typewriter
                .pauseFor(500)
                .typeString("ERROR 404")
                .pauseFor(500)
                .deleteAll(100)
                .typeString("PAGE NOT FOUND")
                .start();
            }}
            options={{
              wrapperClassName: "text-2xl sm:text-5xl",
              cursorClassName: "hidden",
              delay: 100,
            }}
          />

          <TypewriterComponent
            onInit={(typewriter) => {
              typewriter
                .pauseFor(5000)
                .typeString(
                  "We couldn't find what you were looking for. Unfortunately the page you were looking for could not be found. It may be temporarily unavailable, moved or no longer exist."
                )
                .start();
            }}
            options={{
              wrapperClassName: "text-lg sm:text-xl font-medium",
              cursorClassName: "hidden",
              delay: 50,
            }}
          />

          <NavLink to="/">
            <Button type="link">
              <span className="mt-3 text-lg sm:text-xl font-medium">
                Return to home page
              </span>
            </Button>
          </NavLink>
        </div>
      </div>
    </>
  );
}
