import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { myBreadcrumbPath } from "../../Components/Utils/MyBreadcrumb/myBreadcrumbPath";
import TagMember from "../../Components/Utils/Tag/TagMember";
import { setBreadcrumbPath } from "../../redux/layoutSlice/layoutSlice";
import UserDetailPageEditForm from "./UserDetailPageEditForm";

export default function UserDetailPage() {
  const dispatch = useDispatch();
  const { userInfo } = useSelector((state) => state.userSlice);
  const { avatar, name, email, phoneNumber, id } = userInfo;

  const userInfoBlankFields = [
    { name: ["name"], value: name },
    { name: ["email"], value: email },
    { name: ["phoneNumber"], value: phoneNumber },
  ];

  useEffect(() => {
    dispatch(setBreadcrumbPath(myBreadcrumbPath.userDetailPage(name)));
    return () => {
      dispatch(setBreadcrumbPath(null));
    };
  }, [dispatch, name]);

  return (
    <>
      <div className="flex justify-center items-center lg:mt-24">
        <div className="w-full lg:w-4/5 flex flex-col lg:flex-row items-center">
          <div className="w-full lg:w-64 h-64 flex justify-center items-center">
            <TagMember avatar={avatar} marginLeft={0} scale={10} />
          </div>

          <div className="flex-grow">
            <div className="w-68 h-full px-20">
              <UserDetailPageEditForm
                fields={userInfoBlankFields}
                email={email}
                id={id.toString()}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
