import { Button, Form, Input, Tooltip } from "antd";
import { debounce } from "lodash";
import React, { useState } from "react";
import { confirmPassword } from "../../utils/userAction/confirmPassword";
import { deleteUser } from "../../utils/userAction/deleteUser";
import { userEditInfo } from "../../utils/userAction/editUserInfo";
import { useLogin } from "../../utils/userAction/useLogin";
import { useLogout } from "../../utils/userAction/useLogout";

export default function UserDetailPageEditForm({ fields, id, email }) {
  const [isConfirm, setIsConfirm] = useState(false);
  const reLogin = useLogin();
  const logout = useLogout();

  const debounceConfirm = debounce((e) => {
    const { value } = e.target;

    const userInfo = { email, passWord: value };
    const confirmSuccess = () => {
      setIsConfirm(true);
    };

    confirmPassword(userInfo, confirmSuccess);
  }, 1000);

  const handleConfirmPassword = (e) => {
    setIsConfirm(false);
    debounceConfirm(e);
  };

  const handleDeleteUser = () => {
    deleteUser(id, logout);
  };

  const onFinish = (value) => {
    const { email, passWord, confirmPassword, name, phoneNumber } = value;

    const userInput = {
      id,
      email,
      passWord: passWord ? passWord : confirmPassword,
      name,
      phoneNumber,
    };

    const editSuccess = () => {
      const { email, passWord } = userInput;
      const userLoginData = { email, passWord };

      reLogin(userLoginData, true);
    };

    userEditInfo(userInput, editSuccess);
  };

  return (
    <>
      <Form
        name="login"
        layout="vertical"
        requiredMark={false}
        onFinish={onFinish}
        autoComplete="off"
        className="flex flex-col justify-between"
        fields={fields}
      >
        <Form.Item
          label="Email:"
          name="email"
          rules={[
            {
              required: true,
              message: "Please input your email!",
            },
            {
              type: "email",
              message: "Email is not valid!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <div className="grid grid-cols-2 gap-x-3">
          <Form.Item label="New password:" name="passWord">
            <Input.Password />
          </Form.Item>

          <Form.Item
            label="Confirm password:"
            name="confirmPassword"
            tooltip={
              <>
                <p className="mb-1 text-yellow-600">
                  We have to make sure YOU are the owner of this account.
                </p>
                <p className="m-0">So can you please enter your current password?</p>
              </>
            }
          >
            <Input.Password onChange={handleConfirmPassword} />
          </Form.Item>

          <Form.Item
            label="Full name:"
            name="name"
            rules={[
              {
                required: true,
                message: "Please input your full name!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Phone number:"
            name="phoneNumber"
            tooltip={
              <>
                <p className="m-0">
                  Phone number can including space. Like this:
                  <b className="font-normal text-yellow-600"> 012 345 6789</b>
                </p>
              </>
            }
            rules={[
              {
                pattern: "^(([0-9]+[ ]?)*)$",
                message: "Number only!",
              },
            ]}
          >
            <Input />
          </Form.Item>
        </div>

        <Form.Item style={{ marginBottom: 0 }}>
          <Tooltip
            className="w-full block mt-3"
            placement="bottom"
            title={isConfirm ? "" : "You have to confirm password first!"}
          >
            <Button
              style={{ width: "79%", height: 42, marginRight: "1%" }}
              type="primary"
              htmlType="submit"
              disabled={!isConfirm}
            >
              Update your info
            </Button>

            <Button
              style={{ width: "20%", height: 42 }}
              type="primary"
              disabled={!isConfirm}
              danger
              onClick={handleDeleteUser}
            >
              Delete
            </Button>
          </Tooltip>
        </Form.Item>
      </Form>
    </>
  );
}
