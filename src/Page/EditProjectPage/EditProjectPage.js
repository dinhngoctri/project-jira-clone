import { Button } from "antd";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { UploadOutlined, DeleteOutlined } from "@ant-design/icons";
import { useNavigate, useParams } from "react-router-dom";
import { deleteProject } from "../../utils/projectAction/deleteProject";
import FormCreateProject from "../../Components/Utils/Form/FormCreateProject/FormCreateProject";
import { useDispatch } from "react-redux";
import { getAllProject } from "../../utils/projectAction/getAllProject";
import { setBreadcrumbPath } from "../../redux/layoutSlice/layoutSlice";
import { myBreadcrumbPath } from "../../Components/Utils/MyBreadcrumb/myBreadcrumbPath";

const style = { display: "flex", alignItems: "center" };

function Buttons() {
  const history = useNavigate();
  const dispatch = useDispatch();
  const { projectID } = useParams();

  const handleDeleteProject = () => {
    deleteProject(projectID, () => {
      history("/");
      dispatch(getAllProject());
    });
  };

  return (
    <>
      <div className="flex mt-5">
        <Button type="primary" htmlType="submit" className="mr-3" style={style}>
          Update <UploadOutlined />
        </Button>
        <Button type="primary" danger style={style} onClick={handleDeleteProject}>
          Delete <DeleteOutlined />
        </Button>
      </div>
    </>
  );
}

export default function EditProjectPage() {
  const { userInfo } = useSelector((state) => state.userSlice);
  const { name } = userInfo;
  const { projectDetail } = useSelector((state) => state.projectSlice);
  const { id, projectName, projectCategory, description, members } = projectDetail;
  const dispatch = useDispatch();

  const fields = [
    { name: ["projectName"], value: projectName },
    { name: ["categoryId"], value: projectCategory.id },
    { name: ["members"], value: members.map((item) => item.userId) },
  ];

  useEffect(() => {
    dispatch(setBreadcrumbPath(myBreadcrumbPath.editProjectPage(name, id, projectName)));
    return () => {
      dispatch(setBreadcrumbPath(null));
    };
  }, [dispatch, id, name, projectName]);

  return (
    <>
      <FormCreateProject
        isEdit={true}
        projectID={id}
        editData={fields}
        description={description}
        ButtonGroup={<Buttons />}
      />
    </>
  );
}
