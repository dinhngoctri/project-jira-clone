import React, { useEffect } from "react";
import { DragDropContext } from "react-beautiful-dnd";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { formCreateTask } from "../../Components/Utils/MyModal/myModalName";
import { setProjectTaskList } from "../../redux/projectSlice/projectSlice";
import { useShowModal } from "../../utils/layoutAction/useShowModal";
import { updateTaskStatus } from "../../utils/taskAction/updateTaskStatus";
import TagArrayMember from "../../Components/Utils/TagArray/TagArrayMember";
import ProjectPageStatusColumns from "./ProjectPageStatusColumns";
import { setBreadcrumbPath } from "../../redux/layoutSlice/layoutSlice";
import { myBreadcrumbPath } from "../../Components/Utils/MyBreadcrumb/myBreadcrumbPath";

const _ = require("lodash");

export default function ProjectPage() {
  const dispatch = useDispatch();
  const handleShowModal = useShowModal();

  const { userInfo } = useSelector((state) => state.userSlice);
  const { name } = userInfo;
  const { projectDetail, projectTaskList } = useSelector((state) => state.projectSlice);
  const { id, projectName, members } = projectDetail;

  useEffect(() => {
    dispatch(setBreadcrumbPath(myBreadcrumbPath.projectPage(name, id, projectName)));
    return () => {
      dispatch(setBreadcrumbPath(null));
    };
  }, [dispatch, name, id, projectName]);

  const showCreateTaskModal = () => {
    handleShowModal(formCreateTask);
  };

  const onDragEnd = (result) => {
    const { destination, source, draggableId } = result;

    if (!destination) {
      return;
    }

    const { droppableId: currentStatus, index: currentIndex } = destination;
    const { droppableId: previousStatus, index: previousIndex } = source;

    if (currentStatus === previousStatus && currentIndex === previousIndex) {
      return;
    }

    const newTaskList = _.cloneDeep(projectTaskList);

    const previousStatusIndex = newTaskList.findIndex(
      (item) => item.statusId === previousStatus
    );
    // task đang được kéo đi
    const draggedTask = newTaskList[previousStatusIndex].lstTaskDeTail[previousIndex];

    const currentStatusIndex = newTaskList.findIndex(
      (item) => item.statusId === currentStatus
    );

    // cập nhật xoá task khỏi cột status cũ // xoá trước khi thêm để tránh lỗi khi sắp xếp task trên cùng một cột
    newTaskList[previousStatusIndex].lstTaskDeTail.splice(previousIndex, 1);
    // cập nhật thêm task vào cột status mới
    newTaskList[currentStatusIndex].lstTaskDeTail.splice(currentIndex, 0, draggedTask);

    dispatch(setProjectTaskList(newTaskList));

    if (currentStatus === previousStatus) {
      return;
    }
    updateTaskStatus({ taskId: draggableId * 1, statusId: currentStatus });
  };

  return (
    <>
      <DragDropContext onDragEnd={onDragEnd}>
        <p className="mb-4 text-3xl">
          <span className="inline-block mr-2">{projectName}</span>
          <NavLink
            to={`/project-${id}/project-detail`}
            className="text-blue-500 hover:text-blue-400"
          >
            <b className="font-normal text-sm">[see more]</b>
          </NavLink>
        </p>

        <div className="flex flex-wrap mb-10" style={{ marginLeft: 2 }}>
          <TagArrayMember
            projectDetail={projectDetail}
            maxTag={7}
            marginLeft={-2}
            scale={1.25}
          />

          <span
            className="inline-block cursor-pointer transition-all duration-300 text-blue-500 hover:text-blue-400"
            style={{ marginLeft: members <= 0 ? 0 : 12 }}
            onClick={showCreateTaskModal}
          >
            + Create task
          </span>
        </div>

        <div
          className="grid grid-cols-2 lg:grid-cols-4 auto-rows-fr gap-4 lg:gap-7 mb-4"
          style={{ minWidth: 540 }}
        >
          <ProjectPageStatusColumns />
        </div>
      </DragDropContext>
    </>
  );
}
