import React, { useMemo } from "react";
import { Draggable, Droppable } from "react-beautiful-dnd";
import { useSelector } from "react-redux";
import CardTask from "../../Components/Utils/Card/CardTask";

function StatusColumn({ children, statusName }) {
  return (
    <div
      className="flex flex-col items-center px-3 pb-10 space-y-3 bg-white rounded"
      style={{ minHeight: 500 }}
    >
      <p className="w-full m-0 mt-3 font-medium text-gray-600">{statusName}</p>

      {children}
    </div>
  );
}

export default function ProjectPageStatusColumns() {
  const { projectTaskList } = useSelector((state) => state.projectSlice);

  const StatusColumns = useMemo(
    () =>
      projectTaskList &&
      projectTaskList.map((status, index) => {
        const { statusId, statusName, lstTaskDeTail } = status;
        return (
          <Droppable droppableId={statusId} key={index}>
            {(provided) => (
              <div ref={provided.innerRef} {...provided.droppableProps}>
                <StatusColumn statusName={`${statusName} ${lstTaskDeTail.length}`}>
                  {lstTaskDeTail.map((task, index) => {
                    return (
                      <Draggable
                        draggableId={task.taskId.toString()}
                        key={task.taskId}
                        index={index}
                      >
                        {(provided) => (
                          <div
                            className="w-full"
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            <CardTask taskInfo={task} />
                          </div>
                        )}
                      </Draggable>
                    );
                  })}
                  {provided.placeholder}
                </StatusColumn>
              </div>
            )}
          </Droppable>
        );
      }),

    [projectTaskList]
  );

  return <>{StatusColumns}</>;
}
