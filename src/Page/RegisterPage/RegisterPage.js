import React from "react";
import { Button, Form, Input } from "antd";
import { NavLink } from "react-router-dom";
import { register } from "../../utils/userAction/register";
import { useLogin } from "../../utils/userAction/useLogin";

export default function RegisterPage() {
  const login = useLogin();

  const onFinish = (value) => {
    const { email, passWord, name, phoneNumber } = value;
    const userInput = {
      email,
      passWord,
      name,
      phoneNumber,
    };

    const onSuccess = () => {
      login({ email, passWord });
    };
    register(userInput, onSuccess);
  };

  return (
    <>
      <Form
        name="login"
        layout="vertical"
        requiredMark={false}
        onFinish={onFinish}
        autoComplete="off"
        className="w-4/5"
      >
        <Form.Item
          label="Email:"
          name="email"
          tooltip={
            <>
              <p className="m-0">
                No need to use real email, just fake it. Like this:
              </p>
              <p className="text-yellow-600">thisIsABogusMail@gmail.com</p>
            </>
          }
          rules={[
            {
              required: true,
              message: "Please input your email!",
            },
            {
              type: "email",
              message: "Email is not valid!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password:"
          name="passWord"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          label="Full name:"
          name="name"
          rules={[
            {
              required: true,
              message: "Please input your full name!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Phone number:"
          name="phoneNumber"
          tooltip={
            <>
              <p className="m-0">
                Phone number can including space. Like this:
                <b className="font-normal text-yellow-600"> 012 345 6789</b>
              </p>
            </>
          }
          rules={[
            {
              pattern: "^(([0-9]+[ ]?)*)$",
              message: "Number only!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item>
          <Button className="w-full mt-5" type="primary" htmlType="submit">
            Register
          </Button>

          <p className="mt-5 text-center">
            Already have an account?
            <NavLink to="/login"> Login now.</NavLink>
          </p>
        </Form.Item>
      </Form>
    </>
  );
}
