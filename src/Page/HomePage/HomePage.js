import React, { useEffect, useMemo } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import CardProject from "../../Components/Utils/Card/CardProject";
import CardProjectCreateProject from "../../Components/Utils/Card/CardProjectCreateProject";
import CardProjectInfo from "../../Components/Utils/Card/CardProjectInfo";
import { myBreadcrumbPath } from "../../Components/Utils/MyBreadcrumb/myBreadcrumbPath";
import TagProjectCategory from "../../Components/Utils/Tag/TagProjectCategory";
import { setBreadcrumbPath } from "../../redux/layoutSlice/layoutSlice";
import { filterMyProject } from "../../utils/projectAction/filterMyProject";

export default function HomePage() {
  const { allProject } = useSelector((state) => state.projectSlice);
  const { userInfo } = useSelector((state) => state.userSlice);
  const { name } = userInfo;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setBreadcrumbPath(myBreadcrumbPath.homePage(name)));
    return () => {
      dispatch(setBreadcrumbPath(null));
    };
  }, [dispatch, name]);

  const projectInfoCards = useMemo(
    () =>
      allProject &&
      filterMyProject(allProject, userInfo).map((project) => {
        const { id, categoryId, categoryName } = project;
        return (
          <CardProject
            key={id}
            CardContent={<CardProjectInfo projectInfo={project} />}
            cardTitle={<TagProjectCategory category={{ categoryId, categoryName }} />}
          />
        );
      }),
    [allProject, userInfo]
  );

  return (
    <>
      <div className="grid grid-cols-2 lg:grid-cols-4 auto-rows-fr gap-4 mb-10">
        <CardProject
          CardContent={<CardProjectCreateProject />}
          cardTitle="Create new project"
        />
        {projectInfoCards}
      </div>
    </>
  );
}
