import LoginAndRegisterLayout from "../Components/HOCs/LoginAndRegisterLayout/LoginAndRegisterLayout";
import ProjectOverall from "../Components/HOCs/ProjectOverall/ProjectOverall";
import Sider from "../Components/HOCs/Sider/Sider";
import EditProjectPage from "../Page/EditProjectPage/EditProjectPage";
import HomePage from "../Page/HomePage/HomePage";
import LoginPage from "../Page/LoginPage/LoginPage";
import ProjectDetailPage from "../Page/ProjectDetailPage/ProjectDetailPage";
import ProjectPage from "../Page/ProjectPage/ProjectPage";
import RegisterPage from "../Page/RegisterPage/RegisterPage";
import UserDetailPage from "../Page/UserDetailPage/UserDetailPage";

const LoginLayout = (Component) => <LoginAndRegisterLayout Component={<Component />} />;

const SiderLayout = (Component) => <Sider Component={<Component />} />;

const ProjectLayout = (Component) => (
  <Sider Component={<ProjectOverall Component={Component} />} />
);

export const routes = [
  {
    path: "/login",
    element: LoginLayout(LoginPage),
  },
  {
    path: "/register",
    element: LoginLayout(RegisterPage),
  },

  { path: "/", element: SiderLayout(HomePage) },
  { path: "/user-detail", element: SiderLayout(UserDetailPage) },

  { path: "/project-:projectID", element: ProjectLayout(ProjectPage) },
  {
    path: "/project-:projectID/project-detail",
    element: ProjectLayout(ProjectDetailPage),
  },
  {
    path: "/project-:projectID/project-detail/edit-project",
    element: ProjectLayout(EditProjectPage),
  },
];
