import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  allProject: null,
  projectCategory: null,
  projectDetail: null,

  projectTaskList: null,
};

const projectSlice = createSlice({
  name: "projectSlice",
  initialState,
  reducers: {
    setAllProject: (state, { payload }) => {
      state.allProject = payload;
    },

    setProjectCategory: (state, { payload }) => {
      state.projectCategory = payload;
    },

    setProjectDetail: (state, { payload }) => {
      state.projectDetail = payload;

      if (payload) {
        state.projectTaskList = payload.lstTask;
      }
    },

    setProjectTaskList: (state, { payload }) => {
      state.projectTaskList = payload;
    },
  },
});

export const { setAllProject, setProjectCategory, setProjectDetail, setProjectTaskList } =
  projectSlice.actions;

export default projectSlice.reducer;
