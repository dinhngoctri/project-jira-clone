import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  allTaskType: null,
  allPriority: null,
  allStatus: null,

  originalEstimate: null,
  timeSpent: null,
  timeRemaining: null,

  commentList: null,
};

const taskSlice = createSlice({
  name: "taskSlice",
  initialState,
  reducers: {
    setAllTaskType: (state, { payload }) => {
      state.allTaskType = payload;
    },

    setAllPriority: (state, { payload }) => {
      state.allPriority = payload;
    },

    setAllStatus: (state, { payload }) => {
      state.allStatus = payload;
    },

    setTimeTracking: (state, { payload }) => {
      const { originalEstimate, timeTrackingSpent, timeTrackingRemaining } = payload;

      state.originalEstimate = originalEstimate;
      state.timeSpent = timeTrackingSpent;
      state.timeRemaining = timeTrackingRemaining;
    },

    setOriginalEstimate: (state, { payload }) => {
      state.originalEstimate = payload;
    },

    setTimeSpent: (state, { payload }) => {
      state.timeSpent = payload;
    },

    setTimeRemaining: (state, { payload }) => {
      state.timeRemaining = payload;
    },

    setCommentList: (state, { payload }) => {
      state.commentList = payload;
    },
  },
});

export const {
  setAllTaskType,
  setAllPriority,
  setAllStatus,
  setTimeTracking,
  setOriginalEstimate,
  setTimeSpent,
  setTimeRemaining,
  setCommentList,
} = taskSlice.actions;

export default taskSlice.reducer;
