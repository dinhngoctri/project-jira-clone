import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  modal: { modalState: false, modalName: "" },

  drawer: {
    drawerState: false,
    drawerInfo: { drawerName: "", taskId: null, taskTypeDetail: null },
  },

  breadcrumbPath: null,

  isLoading: false,
};

const layoutSlice = createSlice({
  name: "layoutSlice",
  initialState,
  reducers: {
    showModal: (state, { payload }) => {
      state.modal = {
        modalState: true,
        modalName: payload,
      };
    },
    closeModal: (state) => {
      state.modal = {
        modalState: false,
        modalName: "",
      };
    },

    showDrawer: (state, { payload }) => {
      state.drawer = {
        drawerState: true,
        drawerInfo: payload,
      };
    },
    closeDrawer: (state) => {
      state.drawer = {
        drawerState: false,
        drawerInfo: null,
      };
    },

    setBreadcrumbPath: (state, { payload }) => {
      state.breadcrumbPath = payload;
    },

    setLoading: (state, { payload }) => {
      state.isLoading = payload;
    },
  },
});

export const {
  showModal,
  closeModal,
  showDrawer,
  closeDrawer,
  setBreadcrumbPath,
  setLoading,
} = layoutSlice.actions;

export default layoutSlice.reducer;
