import { createSlice } from "@reduxjs/toolkit";
import { userLocalStorage } from "../../services/user/user.localStorage";

const initialState = {
  userInfo: userLocalStorage.userInfo.get(),
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserLoginInfo: (state, { payload }) => {
      state.userInfo = payload;
    },

    removeUserInfo: (state) => {
      state.userInfo = undefined;
    },
  },
});

export const { setUserLoginInfo, removeUserInfo } = userSlice.actions;

export default userSlice.reducer;
