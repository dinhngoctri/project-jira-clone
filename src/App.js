import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { routes } from "./routes/routes";
import { useEffect, useMemo } from "react";
import { useDispatch } from "react-redux";
import { getProjectCategory } from "./utils/projectAction/getProjectCategory";
import { getAllTaskType } from "./utils/taskAction/getAllTaskType";
import { getAllPriority } from "./utils/taskAction/getAllPriority";
import { getAllStatus } from "./utils/taskAction/getAllStatus";
import { getAllProject } from "./utils/projectAction/getAllProject";
import { useSelector } from "react-redux";
import LoadingScreen from "./Components/Utils/LoadingScreen/LoadingScreen";
import NotFoundPage from "./Page/NotFoundPage/NotFoundPage";

function App() {
  const dispatch = useDispatch();
  const { userInfo } = useSelector((state) => state.userSlice);

  const route = useMemo(
    () =>
      routes.map((item, index) => (
        <Route key={index} path={item.path} element={item.element} />
      )),
    []
  );

  useEffect(() => {
    if (userInfo) {
      dispatch(getAllProject());
      dispatch(getProjectCategory());
      dispatch(getAllTaskType());
      dispatch(getAllPriority());
      dispatch(getAllStatus());
    }
  }, [dispatch, userInfo]);

  return (
    <>
      <BrowserRouter>
        <Routes>
          {route}
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </BrowserRouter>

      <LoadingScreen />
    </>
  );
}

export default App;
