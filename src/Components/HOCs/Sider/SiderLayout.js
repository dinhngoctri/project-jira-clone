import React, { useState } from "react";
import { Layout } from "antd";
import MyModal from "../../Utils/MyModal/MyModal";
import MyMenu from "../../Utils/MyMenu/MyMenu";
import { inlineStyle } from "./siderStyleConst";
import MyDrawer from "../../Utils/MyDrawer/MyDrawer";
import MyBreadcrumb from "../../Utils/MyBreadcrumb/MyBreadcrumb";

const { Content, Sider } = Layout;
const { layout01, sider, layout02, content } = inlineStyle;

export default function SiderLayout({ Component }) {
  const [collapsed, setCollapsed] = useState(true);

  return (
    <>
      <Layout style={layout01} hasSider>
        <Sider
          collapsible
          collapsed={collapsed}
          onCollapse={(value) => setCollapsed(value)}
          style={sider}
        >
          <div className="h-full flex flex-col justify-between">
            <MyMenu items={"upperMenuItems"} />
            <MyMenu items={"belowMenuItems"} />
          </div>
        </Sider>
        <Layout className="site-layout" style={layout02}>
          <Content style={content}>
            <div className="xl:container mx-auto">
              <MyBreadcrumb />

              <div className="site-layout-background">{Component}</div>
            </div>
          </Content>
        </Layout>
      </Layout>

      <MyModal />
      <MyDrawer />
    </>
  );
}
