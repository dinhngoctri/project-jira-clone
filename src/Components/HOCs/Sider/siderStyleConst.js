export const inlineStyle = {
  layout01: { minHeight: "100vh" },
  sider: {
    minHeight: "100vh",
    position: "fixed",
    left: 0,
    top: 0,
    bottom: 0,
    overflow: "auto",
    zIndex: 99,
  },
  layout02: {
    minWidth: 560,
    marginLeft: 80,
    overflow: "auto",
  },
  content: {
    margin: "0 16px",
  },
};
