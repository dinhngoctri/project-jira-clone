import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { setProjectDetail } from "../../../redux/projectSlice/projectSlice";
import { getProjectDetail } from "../../../utils/projectAction/getProjectDetail";

export default function ProjectOverall({ Component }) {
  const { projectID } = useParams();
  const dispatch = useDispatch();
  const { projectDetail } = useSelector((state) => state.projectSlice);

  useEffect(() => {
    dispatch(setProjectDetail(null));
    dispatch(getProjectDetail(projectID));
  }, [dispatch, projectID]);

  return <>{projectDetail && <Component />}</>;
}
