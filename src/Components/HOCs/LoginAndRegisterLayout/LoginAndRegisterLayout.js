import React from "react";
import Lottie from "lottie-react";
import loading from "../../../asset/animation/loading.json";

export default function LoginAndRegisterLayout({ Component }) {
  return (
    <>
      <div className="h-screen flex">
        <div className="w-7/12 lg:w-3/5 xl:w-2/3 h-full flex justify-center items-center bg-blue-200">
          <Lottie className="w-2/3" animationData={loading} loop={true} />
        </div>

        <div className="w-5/12 lg:w-2/5 xl:w-1/3 h-full flex justify-center py-10 px-3 lg:p-10">
          {Component}
        </div>
      </div>
    </>
  );
}
