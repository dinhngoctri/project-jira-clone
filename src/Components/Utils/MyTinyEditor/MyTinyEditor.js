import React from "react";
import { Editor } from "@tinymce/tinymce-react";

export default function MyTinyEditor({ editorRef, initialValue, placeholder, height }) {
  return (
    <>
      <Editor
        apiKey="92r0k4njmo36r43csddiwwj59yu9x99fub71i90f8z0y2wsq"
        onInit={(evt, editor) => (editorRef.current = editor)}
        initialValue={initialValue}
        init={{
          placeholder: placeholder,
          height: height,
          menubar: false,

          plugins:
            "importcss image tinydrive searchreplace directionality visualblocks visualchars fullscreen image link media template codesample table charmap pagebreak anchor insertdatetime advlist lists wordcount help charmap quickbars emoticons",

          toolbar:
            "undo redo | " +
            "bold italic underline " +
            "forecolor backcolor emoticons charmap  " +
            "bullist numlist outdent indent  " +
            "alignleft aligncenter alignright alignjustify  " +
            "removeformat codesample   ",

          quickbars_selection_toolbar: "blockquote quicklink",

          content_style:
            "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
        }}
      />
    </>
  );
}
