import { Tooltip } from "antd";
import React, { useMemo } from "react";
import TagMember from "../Tag/TagMember";

export default function TagArrayMember({ projectDetail, maxTag, marginLeft, scale }) {
  const members = useMemo(
    () =>
      projectDetail &&
      projectDetail.members.map((member, index) => {
        const { name, avatar } = member;
        return (
          <Tooltip title={name} key={index}>
            <span>
              <TagMember avatar={avatar} marginLeft={marginLeft} scale={scale} />
            </span>
          </Tooltip>
        );
      }),
    [projectDetail, marginLeft, scale]
  );

  maxTag = maxTag ? maxTag : 5;

  return (
    <>
      {members.length > maxTag ? (
        <>
          {members.slice(0, maxTag)}
          <Tooltip
            title={projectDetail.members.slice(maxTag).map((item, index) => (
              <p className="m-0" key={index}>
                {item.name}
              </p>
            ))}
          >
            <span>
              <TagMember marginLeft={marginLeft} scale={scale} />
            </span>
          </Tooltip>
        </>
      ) : (
        members
      )}
    </>
  );
}
