import React, { useEffect, useState } from "react";
import { ClockCircleOutlined } from "@ant-design/icons";
import { Form, Modal, Progress } from "antd";
import { MyCreateTaskFormItem } from "../Form/FormCreateTask/util.FormItem.formCreateTask";
import { useDispatch, useSelector } from "react-redux";
import { setTimeRemaining, setTimeSpent } from "../../../redux/taskSlice/taskSlice";

const getPercent = (spent, remaining, estimate, setPercent) => {
  spent = spent ? spent : 0;
  remaining = remaining ? remaining : 0;
  estimate = estimate ? estimate : 0;

  estimate === 0 || remaining !== 0
    ? setPercent(Math.round((spent / (spent + remaining)) * 100))
    : setPercent(Math.round((spent / estimate) * 100));
};

export default function TimeTrackingBar() {
  const dispatch = useDispatch();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [percent, setPercent] = useState(0);
  const { originalEstimate, timeSpent, timeRemaining } = useSelector(
    (state) => state.taskSlice
  );

  useEffect(() => {
    getPercent(timeSpent, timeRemaining, originalEstimate, setPercent);
    // eslint-disable-next-line
  }, [originalEstimate]);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const handleTimeTrackingSpentChange = (e) => {
    dispatch(setTimeSpent(e ? e : 0));
    getPercent(e, timeRemaining, originalEstimate, setPercent);
  };
  const handleTimeTrackingRemainingChange = (e) => {
    dispatch(setTimeRemaining(e ? e : 0));
    getPercent(timeSpent, e, originalEstimate, setPercent);
  };

  return (
    <>
      <div
        className="flex items-center gap-x-2 p-3 rounded cursor-pointer transition-all duration-300 hover:bg-slate-200"
        style={{ marginTop: "-12px" }}
        onClick={showModal}
      >
        <ClockCircleOutlined />
        <span className="w-fit inline-block whitespace-nowrap">Time tracking:</span>
        <Progress strokeLinecap="butt" percent={percent} />
      </div>

      <Modal
        title="Time tracking"
        open={isModalOpen}
        onCancel={handleCancel}
        footer={null}
      >
        <div className="flex items-center gap-x-5 mb-10">
          <ClockCircleOutlined />
          <Progress strokeLinecap="butt" percent={percent} />
        </div>

        <Form
          name="create task"
          requiredMark={false}
          autoComplete="off"
          className="flex items-center gap-x-5"
          fields={[
            { name: ["timeTrackingSpent"], value: timeSpent },
            { name: ["timeTrackingRemaining"], value: timeRemaining },
          ]}
        >
          <MyCreateTaskFormItem.TimeTrackingSpent
            onChange={handleTimeTrackingSpentChange}
          />
          <MyCreateTaskFormItem.TimeTrackingRemaining
            onChange={handleTimeTrackingRemainingChange}
          />
        </Form>
      </Modal>
    </>
  );
}
