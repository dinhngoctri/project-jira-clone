import React from "react";
import { useSelector } from "react-redux";
import { SyncLoader } from "react-spinners";

export default function LoadingScreen() {
  const { isLoading } = useSelector((state) => state.layoutSlice);

  return (
    <>
      {isLoading && (
        <div
          className="w-screen h-screen flex justify-center items-center fixed inset-0"
          style={{ backgroundColor: "rgb(0 0 0 / 15%)", zIndex: 999 }}
        >
          <SyncLoader color="#002e5a" loading margin={12} />
        </div>
      )}
    </>
  );
}
