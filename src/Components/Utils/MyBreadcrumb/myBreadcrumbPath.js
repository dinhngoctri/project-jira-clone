const getPath = (title, path) => ({ title, path });

const editProjectPagePath = ({ projectID }) => {
  return getPath("edit project", `/${projectID}/project-detail/edit-project`);
};

const homePagePath = ({ userName }) => {
  return getPath(userName, "/");
};

const projectDetailPagePath = ({ projectName, projectID }) => {
  return getPath(`${projectName} detail`, `/project-${projectID}/project-detail`);
};

const projectPagePath = ({ projectID }) => {
  return getPath(`project ${projectID}`, `/project-${projectID}`);
};

const userDetailPagePath = () => {
  return getPath("edit info", "user-detail");
};

//---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------

export const myBreadcrumbPath = {
  editProjectPage: (userName, projectID, projectName) => [
    homePagePath({ userName }),
    projectPagePath({ projectID }),
    projectDetailPagePath({ projectName, projectID }),
    editProjectPagePath({ projectID }),
  ],

  homePage: (userName) => [homePagePath({ userName })],

  projectDetailPage: (userName, projectID, projectName) => [
    homePagePath({ userName }),
    projectPagePath({ projectID }),
    projectDetailPagePath({ projectName, projectID }),
  ],

  projectPage: (userName, projectID) => [
    homePagePath({ userName }),
    projectPagePath({ projectID }),
  ],

  userDetailPage: (userName) => [homePagePath({ userName }), userDetailPagePath()],
};
