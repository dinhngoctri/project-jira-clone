import { Breadcrumb } from "antd";
import React, { useMemo } from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";

export default function MyBreadcrumb() {
  const { breadcrumbPath } = useSelector((state) => state.layoutSlice);

  const BreadcrumbItems = useMemo(
    () =>
      breadcrumbPath &&
      breadcrumbPath.map((item, index) => {
        const { title, path } = item;

        return (
          <Breadcrumb.Item key={index}>
            <NavLink to={path}> {title}</NavLink>
          </Breadcrumb.Item>
        );
      }),
    [breadcrumbPath]
  );

  return (
    <>
      <Breadcrumb style={{ margin: "16px 0" }}>{BreadcrumbItems}</Breadcrumb>
    </>
  );
}
