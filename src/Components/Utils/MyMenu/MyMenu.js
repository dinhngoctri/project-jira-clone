import React, { useMemo } from "react";
import { Menu } from "antd";
import {
  UserOutlined,
  SearchOutlined,
  PlusOutlined,
  HomeOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { buttonKey, getItem } from "./myMenuUtils";
import { useNavigate, useParams } from "react-router-dom";
import { formCreateTask, searchProjectForm } from "../MyModal/myModalName";
import { useShowModal } from "../../../utils/layoutAction/useShowModal";
import { useLogout } from "../../../utils/userAction/useLogout";
import { useSelector } from "react-redux";

const {
  linkToUserDetailPage,
  linkToHomePage,
  openCreateTaskModal,
  openSearchTaskModal,
  logout,
} = buttonKey;

export default function MyMenu({ items }) {
  const history = useNavigate();
  const { projectID } = useParams();
  const handleShowModal = useShowModal();
  const handleLogout = useLogout();
  const { userInfo } = useSelector((state) => state.userSlice);
  const { name } = userInfo;
  const userName = name > 12 ? name.slice(0, 12) + "..." : name;

  const upperMenuItems = useMemo(
    () => [
      getItem(userName, linkToUserDetailPage, <UserOutlined />, false),
      getItem("HOME", linkToHomePage, <HomeOutlined />, false),
      getItem("SEARCH PROJECT", openSearchTaskModal, <SearchOutlined />, false),
      getItem(
        "CREATE TASK",
        openCreateTaskModal,
        <PlusOutlined />,
        projectID ? false : true
      ),
    ],
    [projectID, userName]
  );
  const belowMenuItems = useMemo(
    () => [getItem("LOGOUT", logout, <LogoutOutlined />, false)],
    []
  );

  const menuItemClicked = ({ key }) => {
    switch (key) {
      case linkToUserDetailPage:
        return history("/user-detail");

      case linkToHomePage:
        return history("/");

      case openSearchTaskModal:
        return handleShowModal(searchProjectForm);

      case openCreateTaskModal:
        // lưu ý khi import modal name
        // modal sẽ truyền modal name từ file myModalName
        // drawer sẽ truyền drawer name từ file myDrawerName
        return handleShowModal(formCreateTask);

      case logout:
        return handleLogout();

      default:
        return () => {};
    }
  };

  const menuItems = useMemo(() => {
    switch (items) {
      case "upperMenuItems":
        return upperMenuItems;

      case "belowMenuItems":
        return belowMenuItems;

      default:
        return [];
    }
  }, [items, upperMenuItems, belowMenuItems]);

  return (
    <>
      <Menu
        theme="dark"
        selectedKeys={["0"]}
        mode="inline"
        items={menuItems}
        onClick={menuItemClicked}
      />
    </>
  );
}
