import React, { useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useMediaQuery } from "react-responsive";
import { Drawer } from "antd";
import { closeDrawer } from "../../../redux/layoutSlice/layoutSlice";
import { formEditTask } from "./myDrawerName";
import TagTaskType from "../Tag/TagTaskType";
import FormEditTask from "../Form/FormEditTask/FormEditTask";

const getDrawerContent = (drawerInfo) => {
  const { drawerName, taskId, taskTypeDetail } = drawerInfo;

  const FormEditTaskTitle = (
    <div className="w-fit">
      <TagTaskType task={taskTypeDetail} taskId={taskId} />
    </div>
  );

  switch (drawerName) {
    case formEditTask:
      return {
        title: FormEditTaskTitle,
        content: <FormEditTask />,
      };

    default:
      return { title: "", content: <></> };
  }
};

export default function MyDrawer() {
  const dispatch = useDispatch();
  const { drawer } = useSelector((state) => state.layoutSlice);
  const { drawerState, drawerInfo } = drawer;
  const isLg = useMediaQuery({ query: "(min-width: 1024px)" });

  const onClose = () => {
    dispatch(closeDrawer());
  };

  const drawerContent = useMemo(() => {
    return drawerInfo ? getDrawerContent(drawerInfo) : { title: "", content: <></> };
  }, [drawerInfo]);

  const { title, content } = drawerContent;

  return (
    <>
      <Drawer
        title={title}
        placement="right"
        onClose={onClose}
        open={drawerState}
        width={isLg ? 736 : 560}
        className="custom-drawer-title"
      >
        <div className="px-1">{content}</div>
      </Drawer>
    </>
  );
}
