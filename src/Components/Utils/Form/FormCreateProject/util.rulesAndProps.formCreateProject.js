export const rules = {
  projectName: [
    {
      required: true,
      message: "Please input your project name!",
    },
  ],
};

export const membersSelectProps = {
  bordered: false,
  mode: "multiple",
  showSearch: true,
  placeholder: "Choose members for your project",
  optionFilterProp: "children",
  filterOption: (input, option) =>
    option.children.toLowerCase().includes(input.toLowerCase()),
  filterSort: (optionA, optionB) =>
    optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase()),
};
