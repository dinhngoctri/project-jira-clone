import { Select } from "antd";
import TagProjectCategory from "../../Tag/TagProjectCategory";

const { Option } = Select;

export const renderCategoryOptions = (projectCategory) =>
  projectCategory.map((item) => {
    const { id } = item;
    return (
      <Option key={id} value={id}>
        <TagProjectCategory category={item} />
      </Option>
    );
  });

export const renderMemberOptions = (users) =>
  users.map((user) => {
    const { userId, email } = user;
    return (
      <Option key={userId} value={userId}>
        {email}
      </Option>
    );
  });
