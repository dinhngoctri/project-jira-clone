import React, { useEffect, useMemo, useRef, useState } from "react";
import { Button, Form, Input, Select } from "antd";
import MyTinyEditor from "../../MyTinyEditor/MyTinyEditor";
import { useDispatch, useSelector } from "react-redux";
import { createProject } from "../../../../utils/projectAction/createProject";
import { updateProject } from "../../../../utils/projectAction/updateProject";
import { getUsers } from "../../../../utils/userAction/getUsers";
import { rules, membersSelectProps } from "./util.rulesAndProps.formCreateProject";
import {
  renderCategoryOptions,
  renderMemberOptions,
} from "./util.render.formCreateProject";
import { projectBlankFields } from "./util.fields.formCreateProject";
import { closeModal } from "../../../../redux/layoutSlice/layoutSlice";
import { getAllProject } from "../../../../utils/projectAction/getAllProject";
import { getProjectDetail } from "../../../../utils/projectAction/getProjectDetail";
import { assignOrRemoveUser } from "../../../../utils/assignOrRemoveUser/assignOrRemoveUser";

const _ = require("lodash");

export default function FormCreateProject({
  isEdit, // isEdit (boolean)
  projectID,
  editData, // editDate (array), trả về một mảng chứa nhiều object để antd lấy làm giá trị khởi tạo cho form
  description, // description (string), trả về một HTML string cho editor TinyMCE
  ButtonGroup, // ButtonGroup (Component), trả về một nhóm nút (thường là nút cập nhật và xoá)
}) {
  const editorRef = useRef(null);
  const dispatch = useDispatch();
  const { projectCategory } = useSelector((state) => state.projectSlice);
  const { modal } = useSelector((state) => state.layoutSlice);
  const [users, setUsers] = useState();
  const [fields, setFields] = useState();

  useEffect(() => {
    setFields(editData ? editData : projectBlankFields);
  }, [editData]);

  useEffect(() => {
    getUsers(setUsers);
  }, []);

  useEffect(() => {
    const { modalState } = modal;
    if (!modalState && !isEdit) {
      setFields(_.cloneDeep(projectBlankFields));
      if (editorRef.current) {
        editorRef.current.setContent("<p></p>");
      }
    }
  }, [modal, isEdit]);

  // render options cho category select và members select
  const categoryOptions = useMemo(
    () => projectCategory && renderCategoryOptions(projectCategory),
    [projectCategory]
  );
  const membersOptions = useMemo(() => users && renderMemberOptions(users), [users]);

  const onFinish = (value) => {
    let description = "";
    if (editorRef.current) {
      // cách lấy dữ liệu từ tinyMCE editor
      description = editorRef.current.getContent();
    }

    const { projectName, categoryId, members } = value;
    const projectInfo = {
      projectName,
      description,
      categoryId,
      alias: "",
    };

    const createSuccess = (successData) => {
      const projectId = successData.id;
      // members && assignOrRemoveUserFromProject(members, [], projectId);
      members && assignOrRemoveUser(members, [], projectId, "project");
      editorRef.current.setContent("<p></p>");
      setFields(_.cloneDeep(projectBlankFields));
      setTimeout(() => {
        dispatch(closeModal());
      }, 300);
    };
    const updateSuccess = (success) => {
      const afterAssignUpdate = () => {
        dispatch(getAllProject());
        dispatch(getProjectDetail(success.data.content.id));
      };

      // lấy dữ liệu thành viên hiện có trong project thông qua mảng fields truyền từ trước
      const memberFields = editData.find((item) => item.name[0] === "members");

      assignOrRemoveUser(
        members,
        memberFields.value,
        projectID,
        "project",
        afterAssignUpdate
      );
    };

    isEdit
      ? updateProject(projectID, projectInfo, updateSuccess)
      : dispatch(createProject(projectInfo, createSuccess));
  };

  return (
    <>
      <Form
        name="create project"
        layout="vertical"
        requiredMark={false}
        onFinish={onFinish}
        autoComplete="off"
        fields={fields}
      >
        <Form.Item
          name="projectName"
          rules={rules.projectName}
          className="custom-explain-error"
        >
          <Input
            bordered={false}
            placeholder="Project name"
            style={{ fontSize: isEdit ? "32px" : "14px" }}
          />
        </Form.Item>

        <Form.Item name="categoryId">
          <Select bordered={false} style={{ width: 180 }}>
            {categoryOptions}
          </Select>
        </Form.Item>

        <Form.Item name="members">
          <Select {...membersSelectProps} maxTagCount={isEdit ? 15 : 5}>
            {membersOptions}
          </Select>
        </Form.Item>

        <MyTinyEditor
          editorRef={editorRef}
          placeholder="Project description"
          initialValue={description ? description : "<p></p>"}
          height={isEdit ? 600 : 300}
        />

        <Form.Item className="flex justify-end">
          {ButtonGroup ? (
            ButtonGroup
          ) : (
            <Button type="primary" htmlType="submit" className="mt-5">
              Create project
            </Button>
          )}
        </Form.Item>
      </Form>
    </>
  );
}
