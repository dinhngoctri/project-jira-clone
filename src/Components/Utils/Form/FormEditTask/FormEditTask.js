import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "antd";
import { UploadOutlined, DeleteOutlined } from "@ant-design/icons";
import { getTaskDetail } from "../../../../utils/taskAction/getTaskDetail";
import FormCreateTask from "../FormCreateTask/FormCreateTask";
import { deleteTask } from "../../../../utils/taskAction/deleteTask";
import { closeDrawer } from "../../../../redux/layoutSlice/layoutSlice";
import { getProjectDetail } from "../../../../utils/projectAction/getProjectDetail";
import { useParams } from "react-router-dom";

const style = { display: "flex", alignItems: "center" };

function Buttons({ taskId }) {
  const dispatch = useDispatch();
  const { projectID } = useParams();

  const deleteSuccess = () => {
    dispatch(closeDrawer());
    dispatch(getProjectDetail(projectID));
  };

  const handleDeleteTask = () => {
    deleteTask(taskId, deleteSuccess);
  };

  return (
    <>
      <div className="flex mt-5">
        <Button type="primary" htmlType="submit" className="mr-3" style={style}>
          Save your changes <UploadOutlined />
        </Button>
        <Button type="primary" danger style={style} onClick={handleDeleteTask}>
          Delete <DeleteOutlined />
        </Button>
      </div>
    </>
  );
}

export default function FormEditTask() {
  const dispatch = useDispatch();
  const [fields, setFields] = useState(null);
  const [description, setDescription] = useState("");
  const { drawer } = useSelector((state) => state.layoutSlice);
  const { taskId } = drawer.drawerInfo;

  const getTaskDetailSuccess = (taskDetail) => {
    const {
      typeId,
      taskName,
      assigness,
      statusId,
      priorityId,
      originalEstimate,
      timeTrackingSpent,
      timeTrackingRemaining,
      description,
    } = taskDetail;

    setFields([
      { name: ["typeId"], value: typeId },
      { name: ["taskName"], value: taskName },
      { name: ["listUserAsign"], value: assigness },
      { name: ["statusId"], value: statusId },
      { name: ["priorityId"], value: priorityId },
      { name: ["originalEstimate"], value: originalEstimate },
      { name: ["timeTrackingSpent"], value: timeTrackingSpent },
      { name: ["timeTrackingRemaining"], value: timeTrackingRemaining },
    ]);

    setDescription(description);
  };

  useEffect(() => {
    dispatch(getTaskDetail(taskId, getTaskDetailSuccess));
  }, [dispatch, taskId]);

  return (
    <>
      {fields && (
        <FormCreateTask
          isEdit={true}
          editData={fields}
          taskId={taskId.toString()}
          description={description}
          ButtonGroup={<Buttons taskId={taskId} />}
        />
      )}
    </>
  );
}
