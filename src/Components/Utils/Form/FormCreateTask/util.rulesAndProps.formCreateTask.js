export const rules = {
  taskName: [
    {
      required: true,
      message: "Please input your task name!",
    },
  ],
};

export const inputNumberProps = {
  min: 0,
  placeholder: "hours",
  bordered: false,
  style: { width: "100%" },
};

export const listUserAsignProps = {
  mode: "multiple",
  showSearch: true,
  optionFilterProp: "children",
  placeholder: "Choose members to handle this task",
  filterOption: (input, option) =>
    option.children.toLowerCase().includes(input.toLowerCase()),
  bordered: false,
};
