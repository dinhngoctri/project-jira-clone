import { Form, Input, InputNumber, Select } from "antd";
import {
  inputNumberProps,
  listUserAsignProps,
  rules,
} from "./util.rulesAndProps.formCreateTask";

export const MyCreateTaskFormItem = {
  TypeId: ({ taskTypeOptions }) => (
    <Form.Item name="typeId">
      <Select bordered={false}>{taskTypeOptions}</Select>
    </Form.Item>
  ),

  TaskName: ({ isEdit }) => (
    <Form.Item name="taskName" className="custom-explain-error" rules={rules.taskName}>
      <Input
        bordered={false}
        placeholder="Task name"
        disabled={isEdit ? true : false}
        style={{ fontSize: isEdit ? "32px" : "14px" }}
      />
    </Form.Item>
  ),

  ListUserAsign: ({ userAssignOptions }) => (
    <Form.Item name="listUserAsign">
      <Select {...listUserAsignProps}>{userAssignOptions}</Select>
    </Form.Item>
  ),

  StatusId: ({ statusOptions }) => (
    <Form.Item name="statusId" label="Task status">
      <Select bordered={false}>{statusOptions}</Select>
    </Form.Item>
  ),

  PriorityId: ({ priorityOptions }) => (
    <Form.Item name="priorityId">
      <Select className="outline-0">{priorityOptions}</Select>
    </Form.Item>
  ),

  OriginalEstimate: ({ onChange }) => (
    <Form.Item name="originalEstimate" label="Original estimate">
      <InputNumber {...inputNumberProps} onChange={onChange} />
    </Form.Item>
  ),

  TimeTrackingSpent: ({ onChange }) => (
    <Form.Item name="timeTrackingSpent" label="Time spent">
      <InputNumber {...inputNumberProps} onChange={onChange} />
    </Form.Item>
  ),

  TimeTrackingRemaining: ({ onChange }) => (
    <Form.Item name="timeTrackingRemaining" label="Time remaining">
      <InputNumber {...inputNumberProps} onChange={onChange} />
    </Form.Item>
  ),
};
