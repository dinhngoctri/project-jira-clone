export const taskBlankFields = [
  { name: ["typeId"], value: 2 },
  { name: ["taskName"], value: "" },
  { name: ["listUserAsign"], value: [] },
  { name: ["statusId"], value: "1" },
  { name: ["priorityId"], value: 2 },
  { name: ["originalEstimate"], value: "" },
  { name: ["timeTrackingSpent"], value: "" },
  { name: ["timeTrackingRemaining"], value: "" },
];
