import React, { useEffect, useMemo, useRef, useState } from "react";
import { Button, Form } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { createTask } from "../../../../utils/taskAction/createTask";
import MyComments from "../../MyComments/MyComments";
import MyTinyEditor from "../../MyTinyEditor/MyTinyEditor";
import { MyCreateTaskFormItem } from "./util.FormItem.formCreateTask";
import {
  renderPriorityOptions,
  renderStatusOptions,
  renderTaskTypeOptions,
  renderUserAssignOptions,
} from "./util.render.formCreateTask";
import TimeTrackingBar from "../../TimeTrackingBar/TimeTrackingBar";
import { setOriginalEstimate } from "../../../../redux/taskSlice/taskSlice";
import { updateTask } from "../../../../utils/taskAction/updateTask";
import { taskBlankFields } from "./util.fields.formCreateTask";
import { closeModal } from "../../../../redux/layoutSlice/layoutSlice";

const _ = require("lodash");
const getValue = (inputValue, defaultValue) => (inputValue ? inputValue : defaultValue);
const getComponent = (condition, componentIfTrue, componentIfFalse = <></>) =>
  condition ? componentIfTrue : componentIfFalse;

const {
  TypeId,
  TaskName,
  ListUserAsign,
  StatusId,
  PriorityId,
  OriginalEstimate,
  TimeTrackingSpent,
  TimeTrackingRemaining,
} = MyCreateTaskFormItem;

export default function FormCreateTask({
  isEdit,
  editData,
  taskId,
  description,
  ButtonGroup,
}) {
  const editorRef = useRef(null);
  const dispatch = useDispatch();
  const { projectID } = useParams();
  const [fields, setFields] = useState();
  const { allTaskType, allPriority, allStatus, timeSpent, timeRemaining } = useSelector(
    (state) => state.taskSlice
  );
  const { projectDetail } = useSelector((state) => state.projectSlice);
  const { modal } = useSelector((state) => state.layoutSlice);
  const { members } = projectDetail ? projectDetail : [];

  useEffect(() => {
    setFields(editData ? editData : taskBlankFields);
  }, [editData]);

  useEffect(() => {
    const { modalState } = modal;
    if (!modalState && !isEdit) {
      setFields(_.cloneDeep(taskBlankFields));
      if (editorRef.current) {
        editorRef.current.setContent("<p></p>");
      }
    }
  }, [modal, isEdit]);

  const handleChangeEstimate = (e) => {
    dispatch(setOriginalEstimate(e ? e : 0));
  };

  const taskTypeOptions = useMemo(
    () => allTaskType && renderTaskTypeOptions(allTaskType),
    [allTaskType]
  );
  const userAssignOptions = useMemo(
    () => members && renderUserAssignOptions(members),
    [members]
  );
  const statusOptions = useMemo(
    () => allStatus && renderStatusOptions(allStatus),
    [allStatus]
  );
  const priorityOptions = useMemo(
    () => allPriority && renderPriorityOptions(allPriority),
    [allPriority]
  );

  const onFinish = (value) => {
    let description = "";
    if (editorRef.current) {
      // cách lấy dữ liệu từ tinyMCE editor
      description = editorRef.current.getContent();
    }

    const {
      typeId,
      taskName,
      statusId,
      priorityId,
      originalEstimate,
      timeTrackingRemaining,
      timeTrackingSpent,
    } = value;

    const taskInfo = {
      typeId: getValue(typeId, 2),
      taskName,
      statusId: getValue(statusId, "1"),
      priorityId: getValue(priorityId, 2),
      originalEstimate: getValue(originalEstimate, 0),
      listUserAsign: [],
      description,
      projectId: projectID * 1,
    };

    const taskInfoCreate = {
      ...taskInfo,
      timeTrackingRemaining: getValue(timeTrackingRemaining, 0),
      timeTrackingSpent: getValue(timeTrackingSpent, 0),
    };

    const taskInfoEdit = {
      ...taskInfo,
      taskId: taskId,
      timeTrackingRemaining: getValue(timeRemaining, 0),
      timeTrackingSpent: getValue(timeSpent, 0),
    };

    const createTaskSuccess = () => {
      setFields(_.cloneDeep(taskBlankFields));
      editorRef.current.setContent("<p></p>");
      dispatch(closeModal());
    };

    isEdit
      ? dispatch(updateTask(taskInfoEdit))
      : dispatch(createTask(taskInfoCreate, createTaskSuccess));
  };

  return (
    <>
      <Form
        name="create task"
        requiredMark={false}
        onFinish={onFinish}
        autoComplete="off"
        fields={fields}
      >
        {getComponent(!isEdit, <TypeId taskTypeOptions={taskTypeOptions} />)}

        <TaskName isEdit={isEdit} />

        {getComponent(!isEdit, <ListUserAsign userAssignOptions={userAssignOptions} />)}

        <MyTinyEditor
          editorRef={editorRef}
          placeholder="Task description"
          height={300}
          initialValue={description ? description : "<p></p>"}
        />

        <div
          className={`grid ${isEdit ? "grid-cols-2 gap-5" : "grid-cols-1"}`}
          style={{ marginTop: isEdit ? 48 : 32, marginBottom: isEdit ? 24 : 0 }}
        >
          <div>
            {getComponent(
              isEdit,
              <>
                <TypeId taskTypeOptions={taskTypeOptions} />
                <ListUserAsign userAssignOptions={userAssignOptions} />
              </>
            )}
          </div>

          <div style={{ padding: isEdit ? "0px 11px" : "" }}>
            <StatusId statusOptions={statusOptions} />
            <PriorityId priorityOptions={priorityOptions} />
          </div>
        </div>

        <div style={{ marginBottom: isEdit ? 36 : 0 }}>
          <div className={`${isEdit ? "px-3" : ""}`}>
            <OriginalEstimate onChange={isEdit ? handleChangeEstimate : () => {}} />
          </div>
          {getComponent(
            isEdit,
            <TimeTrackingBar />,
            <div className="grid grid-cols-2 gap-5">
              <TimeTrackingSpent />
              <TimeTrackingRemaining />
            </div>
          )}
        </div>

        <Form.Item className="flex justify-end" style={{ marginBottom: isEdit ? 48 : 0 }}>
          {ButtonGroup ? (
            ButtonGroup
          ) : (
            <Button type="primary" htmlType="submit" className="mt-5">
              Create task
            </Button>
          )}
        </Form.Item>
      </Form>

      {getComponent(isEdit, <MyComments taskId={taskId} />)}
    </>
  );
}
