import { Select } from "antd";
import TagPriority from "../../Tag/TagPriority";
import TagTaskType from "../../Tag/TagTaskType";

const { Option } = Select;

export const renderTaskTypeOptions = (allTaskType) =>
  allTaskType.map((item, index) => (
    <Option key={index} value={item.id}>
      <TagTaskType task={item} />
    </Option>
  ));

export const renderUserAssignOptions = (members) =>
  members.map((member, index) => (
    <Option key={index} value={member.userId}>
      {member.name}
    </Option>
  ));

export const renderStatusOptions = (allStatus) =>
  allStatus.map((status, index) => (
    <Option key={index} value={status.statusId}>
      {status.statusName}
    </Option>
  ));

export const renderPriorityOptions = (allPriority) =>
  allPriority.map((priority, index) => (
    <Option key={index} value={priority.priorityId}>
      <TagPriority taskPriority={priority} />
    </Option>
  ));
