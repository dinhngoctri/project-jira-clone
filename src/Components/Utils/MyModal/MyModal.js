import React, { useMemo } from "react";
import { Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { closeModal } from "../../../redux/layoutSlice/layoutSlice";
import { formCreateProject, formCreateTask, searchProjectForm } from "./myModalName";
import FormCreateProject from "../Form/FormCreateProject/FormCreateProject";
import FormCreateTask from "../Form/FormCreateTask/FormCreateTask";
import SearchProject from "../Search/SearchProject/SearchProject";

export default function MyModal() {
  const { modal } = useSelector((state) => state.layoutSlice);
  const { modalName, modalState } = modal;
  const dispatch = useDispatch();

  const handleCancel = () => {
    dispatch(closeModal());
  };

  const modalContent = useMemo(() => {
    switch (modalName) {
      case formCreateTask:
        return { title: "Create task", content: <FormCreateTask /> };

      case formCreateProject:
        return { title: "Create project", content: <FormCreateProject /> };

      case searchProjectForm:
        return { title: "Search project", content: <SearchProject /> };

      default:
        return { title: "Modal", content: <></> };
    }
  }, [modalName]);

  const { title, content } = modalContent;

  return (
    <>
      <Modal
        title={title}
        open={modalState}
        onCancel={handleCancel}
        footer={null}
        className="custom-modal"
      >
        {content}
      </Modal>
    </>
  );
}
