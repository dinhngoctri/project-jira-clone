import React from "react";
import { useSelector } from "react-redux";
import MyCommentsInput from "./MyCommentsInput";
import MyCommentsOutput from "./MyCommentsOutput";

export default function MyComments({ taskId }) {
  const { userInfo } = useSelector((state) => state.userSlice);
  const { avatar, name } = userInfo;

  return (
    <>
      <hr />
      <div className="py-12">
        <p className="font-medium">Comment</p>

        <div className="flex flex-wrap mb-3">
          <img src={avatar} alt="..." className="w-6 h-6 mr-2 rounded-full" />
          <span>{name}</span>
        </div>

        <div className="grid grid-cols-2 gap-10">
          <MyCommentsInput taskId={taskId} />
          <MyCommentsOutput taskId={taskId} />
        </div>
      </div>
    </>
  );
}
