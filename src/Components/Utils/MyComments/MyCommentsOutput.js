import React, { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllComment } from "../../../utils/taskAction/getAllComment";
import TagComment from "../Tag/TagComment";

export default function MyCommentsOutput({ taskId }) {
  const dispatch = useDispatch();
  const { commentList } = useSelector((state) => state.taskSlice);

  useEffect(() => {
    dispatch(getAllComment(taskId));
  }, [dispatch, taskId]);

  const comments = useMemo(
    () =>
      commentList &&
      commentList.map((commentData) => {
        const { id, user, contentComment } = commentData;
        const { avatar, name, userId } = user;
        return (
          <TagComment
            key={id}
            id={id}
            avatar={avatar}
            name={name}
            contentComment={contentComment}
            taskId={taskId}
            userId={userId}
          />
        );
      }),
    [commentList, taskId]
  );

  return (
    <>
      {comments && (
        <div
          className="p-3 space-y-3 rounded-md shadow-lg overflow-scroll transition-all duration-100"
          style={{ maxHeight: 300, opacity: comments.length > 0 ? 1 : 0 }}
        >
          {comments}
        </div>
      )}
    </>
  );
}
