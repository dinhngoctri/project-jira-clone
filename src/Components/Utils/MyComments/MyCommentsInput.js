import React, { useState } from "react";
import { Button, Input } from "antd";
import { insertComment } from "../../../utils/taskAction/insertComment";
import { useDispatch } from "react-redux";

const { TextArea } = Input;

export default function MyCommentsInput({ taskId }) {
  const [contentComment, setContentComment] = useState("");
  const dispatch = useDispatch();

  const onChange = (e) => {
    setContentComment(e.target.value);
  };

  const onFinish = () => {
    const commentData = {
      taskId,
      contentComment,
    };

    if (
      contentComment.length > 0 &&
      contentComment.match(/^(\s*[a-zA-Z0-9!@#$%^&*)(+=.,?_\-}{\\[\]/`~:;'"|<>]+\s*)+$/)
    ) {
      dispatch(insertComment(commentData));
      setContentComment("");
    }
  };

  return (
    <>
      <div className="space-y-3">
        <TextArea
          placeholder="Add a comments..."
          autoSize={{
            minRows: 1,
            maxRows: 7,
          }}
          value={contentComment}
          onChange={onChange}
        />
        <Button type="primary" onClick={onFinish} className="w-full">
          Post
        </Button>
      </div>
    </>
  );
}
