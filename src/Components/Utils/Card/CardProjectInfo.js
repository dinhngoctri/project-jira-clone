import React from "react";
import { NavLink } from "react-router-dom";
import { getTextFromHtmlString } from "../../../utils/getTextFromHtmlString/getTextFromHtmlString";
import { cardProjectInfoStyle } from "./cardProjectUtils";
import TagArrayMember from "../TagArray/TagArrayMember";

export default function CardProjectInfo({ projectInfo }) {
  const { id, projectName, members, description } = projectInfo;

  const descriptionText = getTextFromHtmlString(description);

  return (
    <>
      <NavLink to={`/project-${id}`} className={cardProjectInfoStyle.className.navLink}>
        <div>
          <b>Project: </b>
          <span>
            {projectName.length > 30 ? projectName.slice(0, 30) + "..." : projectName}
          </span>
        </div>

        <div className="flex">
          <b style={{ marginRight: 8 }}>Members: </b>
          <div className="flex">
            {members.length > 0 ? (
              <TagArrayMember projectDetail={projectInfo} maxTag={4} />
            ) : (
              <span>creator only</span>
            )}
          </div>
        </div>

        <div>
          <p>
            <b>Description: </b>
            {descriptionText.length === 0
              ? "lack of description"
              : descriptionText.length > 50
              ? descriptionText.slice(0, 50) + "..."
              : descriptionText}
          </p>
        </div>
      </NavLink>
    </>
  );
}
