export const cardProjectInfoStyle = {
  className: {
    navLink:
      "w-full h-full block pt-4 px-4 space-y-1 border-b-2 rounded-t text-black transition-all duration-300 cursor-pointer hover:border-white hover:bg-blue-300 hover:text-white",
  },
};
