import React from "react";

const cardStyle = { minWidth: 220, minHeight: 192 };

export default function CardProject({ CardContent, cardTitle }) {
  return (
    <>
      <div style={cardStyle} className="w-full flex flex-col justify-between pb-2 rounded bg-white">
        <div className="w-full mb-4 flex-grow">{CardContent}</div>
        <div className="h-9 m-0 px-4 ml-auto whitespace-nowrap text-right">{cardTitle}</div>
      </div>
    </>
  );
}
