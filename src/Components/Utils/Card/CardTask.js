import React from "react";
import { useShowDrawer } from "../../../utils/layoutAction/useShowDrawer";
import { formEditTask } from "../MyDrawer/myDrawerName";
import TagPriority from "../Tag/TagPriority";
import TagTaskType from "../Tag/TagTaskType";

const className =
  "w-full " +
  "flex flex-col justify-between " +
  "px-3 py-5 " +
  "border-2 border-gray-300 rounded " +
  "shadow-md bg-white " +
  "cursor-pointer transition-all duration-300 " +
  "hover:bg-blue-100 hover:scale-105 ";

export default function CardTask({ taskInfo }) {
  const { taskId, taskName, taskTypeDetail, priorityTask } = taskInfo;
  const showDrawer = useShowDrawer();

  const handleShowDrawer = () => {
    // lưu ý khi import drawer name
    // drawer sẽ truyền drawer name từ file myDrawerName
    // modal sẽ truyền modal name từ file myModalName
    showDrawer({ drawerName: formEditTask, taskId, taskTypeDetail });
  };

  return (
    <>
      <div className={className} onClick={handleShowDrawer}>
        <div style={{ minHeight: 32 }}>
          <p>{taskName.length > 120 ? taskName.slice(0, 120) + "..." : taskName}</p>
        </div>

        <div className="flex justify-between mt-3 gap-1">
          <TagTaskType task={taskTypeDetail} hideTaskName={true} />
          <TagPriority taskPriority={priorityTask} hidePriority={false} />
        </div>
      </div>
    </>
  );
}
