import React from "react";
import { PlusCircleOutlined } from "@ant-design/icons";
import { useShowModal } from "../../../utils/layoutAction/useShowModal";
import { formCreateProject } from "../MyModal/myModalName";

export default function CardProjectCreateProject() {
  const showModal = useShowModal();
  const handleShowModal = () => {
    showModal(formCreateProject);
  };

  return (
    <>
      <div
        className="h-full flex justify-center items-center border-b-2 cursor-pointer transition-all duration-300 hover:border-blue-300 hover:bg-blue-300 hover:text-white"
        onClick={handleShowModal}
      >
        <PlusCircleOutlined className="text-4xl" />
      </div>
    </>
  );
}
