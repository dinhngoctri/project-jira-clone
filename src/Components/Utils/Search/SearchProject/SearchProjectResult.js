import React, { useCallback, useMemo } from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { closeModal } from "../../../../redux/layoutSlice/layoutSlice";
import { highlights } from "../../../../utils/highlight/highlight";
import TagProjectCategory from "../../Tag/TagProjectCategory";

export default function SearchProjectResult({
  searchResult,
  searchValue,
  highlightIndex,
}) {
  const dispatch = useDispatch();

  const closeSearchModal = useCallback(() => {
    dispatch(closeModal());
  }, [dispatch]);

  const projects = useMemo(
    () =>
      searchResult &&
      searchResult.map((item, index) => {
        const { id, projectName, categoryId, categoryName } = item;
        return (
          <NavLink
            key={id}
            to={`/project-${id}`}
            className="block"
            onClick={closeSearchModal}
          >
            <div className="w-full px-4 py-3 space-y-3 rounded bg-gray-50 shadow text-gray-500 transition-all duration-300 hover:bg-gray-100 hover:text-blue-400 ">
              <p className="m-0">{`Project id: ${id}`}</p>
              <p className="m-0">{highlights(projectName, highlightIndex[index])}</p>
              <TagProjectCategory category={{ categoryId, categoryName }} />
            </div>
          </NavLink>
        );
      }),
    [searchResult, highlightIndex, closeSearchModal]
  );

  return (
    <>
      <div className="grid grid-cols-2 gap-5 mt-4">{projects}</div>
    </>
  );
}
