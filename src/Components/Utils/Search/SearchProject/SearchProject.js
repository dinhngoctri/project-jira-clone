import React, { useCallback, useState } from "react";
import SearchProjectForm from "./SearchProjectForm";
import SearchProjectResult from "./SearchProjectResult";

export default function SearchProject() {
  const [searchResult, setSearchResult] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [highlightIndex, setHighlightIndex] = useState();

  const setResult = useCallback((value) => {
    setSearchResult(value);
  }, []);

  const setValue = useCallback((value) => {
    setSearchValue(value);
  }, []);

  const setHighlight = useCallback((value) => {
    setHighlightIndex(value);
  }, []);

  return (
    <>
      <SearchProjectForm
        setResult={setResult}
        setValue={setValue}
        setHighlight={setHighlight}
      />
      <SearchProjectResult
        searchResult={searchResult}
        searchValue={searchValue}
        highlightIndex={highlightIndex}
      />
    </>
  );
}
