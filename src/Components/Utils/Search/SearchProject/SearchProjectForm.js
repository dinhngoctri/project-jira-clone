import React, { useMemo } from "react";
import { Input } from "antd";
import { debounce } from "lodash";
import { useSelector } from "react-redux";
import { filterMyProject } from "../../../../utils/projectAction/filterMyProject";
import { aliasConvert } from "../../../../utils/aliasConvert/aliasConvert";

const convertSearchString = (string) => aliasConvert(string).toLowerCase();

export default function SearchProjectForm({ setResult, setValue, setHighlight }) {
  const { allProject } = useSelector((state) => state.projectSlice);
  const { userInfo } = useSelector((state) => state.userSlice);

  const filteredProjectList = useMemo(
    () => filterMyProject(allProject, userInfo),
    [allProject, userInfo]
  );

  const debounceSearch = debounce((e) => {
    const { value } = e.target;

    if (!value || value === " ") {
      setResult([]);
      return;
    }

    const projects = filteredProjectList.filter((item) => {
      return (
        convertSearchString(item.projectName).includes(convertSearchString(value)) ||
        item.id.toString().includes(value)
      );
    });

    const highlightIndex = projects.map((item) => {
      return {
        start: convertSearchString(item.projectName).indexOf(convertSearchString(value)),
        end:
          convertSearchString(item.projectName).indexOf(convertSearchString(value)) +
          convertSearchString(value).length -
          1,
      };
    });

    setValue(value);
    setHighlight(highlightIndex);
    setResult(projects);
  }, 1);

  const onChange = (e) => {
    debounceSearch(e);
  };

  return (
    <>
      <Input
        style={{ paddingBottom: 16, fontSize: 18 }}
        bordered={false}
        placeholder="Input project name or project id"
        onChange={onChange}
      />
    </>
  );
}
