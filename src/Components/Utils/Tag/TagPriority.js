import React, { useMemo } from "react";
import { ArrowUpOutlined, MinusSquareOutlined, ArrowDownOutlined } from "@ant-design/icons";

export default function TagPriority({ taskPriority }) {
  const { priorityId, priority } = taskPriority;

  const tag = useMemo(() => {
    switch (priorityId) {
      case 1:
        return (
          <div className="flex items-center text-red-600">
            <ArrowUpOutlined />
          </div>
        );

      case 2:
        return (
          <div className="flex items-center text-yellow-500">
            <MinusSquareOutlined />
          </div>
        );

      case 3:
        return (
          <div className="flex items-center text-green-600">
            <ArrowDownOutlined />
          </div>
        );

      case 4:
        return (
          <div className="flex items-center text-green-600">
            <ArrowDownOutlined />
            <ArrowDownOutlined />
          </div>
        );

      default:
        return {
          icon: "",
        };
    }
  }, [priorityId]);

  return (
    <>
      <div className="flex items-center gap-2">
        <span>{priority.toLowerCase()} priority</span> {tag}
      </div>
    </>
  );
}
