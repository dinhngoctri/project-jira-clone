import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteComment } from "../../../utils/taskAction/deleteComment";
import { updateComment } from "../../../utils/taskAction/updateComment";

export default function TagComment({ id, avatar, name, contentComment, taskId, userId }) {
  const [isEdit, setIsEdit] = useState(false);
  const [content, setContent] = useState(contentComment);
  const { userInfo } = useSelector((state) => state.userSlice);
  const dispatch = useDispatch();

  const onChange = (e) => {
    setContent(e.target.value);
  };

  const onFinish = () => {
    setIsEdit(false);
    dispatch(updateComment(id, content));
  };

  const onCancel = () => {
    setIsEdit(false);
    setContent(contentComment);
  };

  const onDelete = () => {
    dispatch(deleteComment(id, taskId));
  };

  return (
    <div>
      <div className="flex flex-wrap mb-2">
        <img src={avatar} alt="..." className="w-6 h-6 mr-2 rounded-full" />
        <span>{name}</span>
      </div>

      {isEdit ? (
        <>
          <input
            className="w-full px-2 py-1 mb-1 rounded-md border-2 border-blue-300 outline-none"
            value={content}
            onChange={onChange}
          />
          <p className="flex justify-end px-2 space-x-2 text-xs">
            <span
              className="transition-all duration-300 cursor-pointer hover:text-blue-500"
              onClick={onFinish}
            >
              Save
            </span>
            <span
              className="transition-all duration-300 cursor-pointer hover:text-red-500"
              onClick={onCancel}
            >
              Cancel
            </span>
          </p>
        </>
      ) : (
        <>
          <p
            className="px-2 py-1 mb-1 rounded-md border-2 break-words"
            style={{ marginBottom: userId !== userInfo.id && 32 }}
          >
            {content}
          </p>
          {userId === userInfo.id && (
            <p className="flex justify-end px-2 space-x-2 text-xs">
              <span
                className="transition-all duration-300 cursor-pointer hover:text-blue-500"
                onClick={() => {
                  setIsEdit(true);
                }}
              >
                Edit
              </span>
              <span
                className="transition-all duration-300 cursor-pointer hover:text-red-500"
                onClick={onDelete}
              >
                Delete
              </span>
            </p>
          )}
        </>
      )}
    </div>
  );
}
