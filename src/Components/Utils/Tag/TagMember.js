import React from "react";

const className =
  "w-6 h-6 flex justify-center items-center border-2 border-gray-100 rounded-full bg-white overflow-hidden";

export default function TagMember({ avatar, marginLeft, scale }) {
  return (
    <>
      <div
        className={className}
        style={{
          marginLeft: marginLeft ? marginLeft : -6,
          transform: scale ? `scale(${scale})` : `scale(1)`,
        }}
      >
        {avatar === "..." || !avatar ? (
          <span className="w-full block bg-gray-300 text-center text-black">...</span>
        ) : (
          <img src={avatar} alt="..." className="object-cover" />
        )}
      </div>
    </>
  );
}
