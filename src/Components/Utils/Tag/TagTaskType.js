import React, { useMemo } from "react";
import { FileExclamationOutlined, FileAddOutlined } from "@ant-design/icons";
import { Tag } from "antd";

export default function TagTaskType({ task, hideTaskName, taskId }) {
  const { id, taskType } = task;

  const tag = useMemo(() => {
    switch (id) {
      case 1:
        return {
          icon: <FileExclamationOutlined />,
          color: "error",
        };

      case 2:
        return {
          icon: <FileAddOutlined />,
          color: "success",
        };

      default:
        return {
          icon: "",
          color: "",
        };
    }
  }, [id]);

  const { icon, color } = tag;

  return (
    <>
      <Tag color={color} className="items-center " style={{ display: "flex", padding: 4 }}>
        {icon}

        {hideTaskName ? (
          ""
        ) : (
          <span>
            {taskType} {taskId ? <span className="text-black font-normal">- {taskId}</span> : ""}
          </span>
        )}
      </Tag>
    </>
  );
}
